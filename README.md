
# Vue Todo APP

A simple todo app made with vue to be evaluated as the vue assignment of P2P.


## Tech Stack

**Client:** Vue 3, Vite, TailwindCSS, json-server
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/joao.zaniolo/vue-assignment
```

Go to the project directory

```bash
  cd vue-assignment
```

Install dependencies

```bash
  npm install
```

Start json-server

```bash
  npm run server
```

In another terminal, start vue project

```bash
  npm run dev
```

## Authors

- [@jvzaniolo](https://www.github.com/jvzaniolo)

  
